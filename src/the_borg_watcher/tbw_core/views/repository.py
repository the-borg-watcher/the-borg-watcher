"""
this module provides Repository related views
"""

import datetime

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView, ListView
from django.views.generic.list import MultipleObjectMixin

from django_q.tasks import async_task
from django_q.models import Schedule

from tbw_core.models import Repository, RepositorySchedule
from tbw_core.services import Borg, BorgError
from tbw_core.forms import RepositoryForm, ImportRepositoryForm

# pylint: disable=R0901 # too many ancestor

class AllRepositoriesView(LoginRequiredMixin, ListView):
    """ this view displays all repositories """
    template_name = 'tbw_core/all_repositories.html'

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Repository.objects.all()
        return self.user.repository_set.all()

class RepositoryView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    """ this view provides details about a repository """
    template_name = 'tbw_core/repository.html'
    context_object_name = 'repository'
    model = Repository

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.paginate_by = request.user.settings.archives_per_page
        return super().get(request, args, kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Repository.objects.all()
        return self.user.repository_set.all()

    def get_context_data(self, **kwargs):
        object_list = self.object.archive_set.order_by('name')
        context = super().get_context_data(object_list=object_list, **kwargs)
        return context

class ImportRepositoryView(LoginRequiredMixin, CreateView):
    """ create a new repository """
    model = Repository
    template_name = 'tbw_core/import_repository.html'
    form_class = ImportRepositoryForm

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        if "cancel" in request.POST:
            return HttpResponseRedirect(reverse('index'))

        self.user = request.user

        response = super().post(request, *args, **kwargs)
        if response.status_code == 302:
            messages.add_message(request, messages.INFO, 'Repository is being scanned !')

        return response

    def form_valid(self, form):
        """ override form_valid() to create the object from from_location() """
        try:
            new = Borg(self.user.settings).import_repository(
                self.user,
                form.cleaned_data,
            )
            new.save()

            # trigg an initial scan
            async_task('tbw_core.tasks.scan_repository_by_id', new.id, self.user.settings.id)

            # schedule a daily scan of the new repository, first asap
            RepositorySchedule.objects.create(func='tbw_core.tasks.scan_repository_by_id',
                args="f{new.id}, {self.user.settings.id}",
                schedule_type=Schedule.DAILY,
                repository=new,
                next_run=datetime.date.today() + datetime.timedelta(days=1)
            )

            return HttpResponseRedirect(reverse('repository', args=(new.id,)))
        except BorgError as error:
            form.add_error('location', error)
            return super().form_invalid(form)

class NewRepositoryView(LoginRequiredMixin, CreateView):
    """ create a new repository """
    model = Repository
    template_name = 'tbw_core/new_repository.html'
    form_class = RepositoryForm

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        if "cancel" in request.POST:
            return HttpResponseRedirect(reverse('index'))

        self.user = request.user

        response = super().post(request, *args, **kwargs)
        if response.status_code == 302:
            messages.add_message(request, messages.INFO, 'Repository is being scanned !')

        return response

    def form_valid(self, form):
        """ override form_valid() to create the object from from_location() """
        try:
            new = Borg(self.user.settings).new_repository(
                self.user,
                form.cleaned_data,
            )
            new.save()

            # trigg an initial scan
            async_task('tbw_core.tasks.scan_repository_by_id', new.id, self.user.settings.id)

            # schedule a daily scan of the new repository, first asap
            RepositorySchedule.objects.create(func='tbw_core.tasks.scan_repository_by_id',
                args="{}, {}".format(new.id, self.user.settings.id),
                schedule_type=Schedule.DAILY,
                repository=new,
                next_run=datetime.date.today() + datetime.timedelta(days=1)
            )

            return HttpResponseRedirect(reverse('repository', args=(new.id,)))
        except BorgError as error:
            form.add_error('location', error)
            return super().form_invalid(form)

class EditRepositoryView(LoginRequiredMixin, UpdateView):
    """ edit a repository """
    model = Repository
    form_class = ImportRepositoryForm
    template_name = 'tbw_core/edit_repository.html'
    success_url = reverse_lazy('all_repositories')

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Repository.objects.all()
        return self.user.repository_set.all()

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        self.user = request.user

        if "cancel" in request.POST:
            return HttpResponseRedirect(self.success_url)
        return super().post(request, *args, **kwargs)

class DeleteRepositoryView(LoginRequiredMixin, DeleteView):
    """ delete a repository """
    model = Repository
    template_name = 'tbw_core/delete_repository.html'
    success_url = reverse_lazy('all_repositories')
    delete_on_disk = False

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Repository.objects.all()
        return self.user.repository_set.all()

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        self.user = request.user

        if 'cancel' in request.POST:
            return HttpResponseRedirect(self.success_url)
        if 'delete_on_disk' in request.POST:
            self.delete_on_disk = request.POST['delete_on_disk'] == 'on'
        return super().post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()

        messages.add_message(request, messages.INFO, f'Repository {self.object.location} is being deleted !')

        async_task('tbw_core.tasks.delete_repository_by_id',
                self.object.id,
                request.user.settings.id,
                self.delete_on_disk,
                q_options={'timeout': 60},
            )

        return HttpResponseRedirect(self.success_url)

class ScanRepositoryView(LoginRequiredMixin, DetailView):
    """ scan a repository """
    model = Repository
    slug_url_kwarg = 'archive_id'

    def get(self, request, *args, **kwargs):
        """ override get() to call list_archives() """
        self.user = request.user
        self.object = self.get_object()

        # update archive list
        try:
            Borg(request.user.settings).scan_repository(self.object)

        except BorgError as error:
            messages.add_message(request, messages.ERROR, f'Borg Error: {str(error)}', 'danger')

        return HttpResponseRedirect(reverse('repository', args=(self.object.pk,)))

    def get_queryset(self):
        if self.user.is_superuser:
            return Repository.objects.all()
        return self.user.repository_set.all()


from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from tbw_core.models import Repository

# pylint: disable=R0901 # too many ancestor

class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'tbw_core/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.user.is_superuser:
            context['repository_list'] = Repository.objects.all()
        else:
            context['repository_list'] = self.user.repository_set.all()
        return context

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, args, kwargs)

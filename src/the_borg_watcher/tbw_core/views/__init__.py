"""
this module contains all views
"""
from .archive import *
from .index import *
from .repository import *
from .settings import *

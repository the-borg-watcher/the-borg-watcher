"""
this module contains Archive related views
"""
import os

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import DetailView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from django_q.tasks import async_task

from tbw_core.models import Archive, File
from tbw_core.services import Borg, BorgError

# pylint: disable=R0901 # too many ancestor

class ArchiveView(LoginRequiredMixin, DetailView, MultipleObjectMixin):
    """ this view provides details about the Archive provided by its pk """
    template_name = 'tbw_core/archive.html'
    context_object_name = 'archive'
    model = Archive
    file_id = None

    def get(self, request, *args, **kwargs):
        self.user = request.user
        self.file_id = request.GET.get('file')
        self.paginate_by = request.user.settings.files_per_page
        return super().get(request, args, kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Archive.objects.all()
        return Archive.objects.filter(repository__user=self.user)

    def get_context_data(self, **kwargs):
        """ override get_context_data to insert object_list """
        if self.file_id is None:
            # no file requested, display all root files
            object_list = self.object.get_roots()
        else:
            # display this directory content
            file = self.object.file_set.get(id=self.file_id)
            object_list = file.get_children()
            kwargs['path'] = file.path
            kwargs['parent_pk'] = file.get_parent_pk()
            # getvars contains additional GET variables added after pagination
            kwargs['getvars'] = f"&file={self.file_id}"

        # append deep_scan info
        kwargs['deep_scan'] = self.object.repository.deep_scan

        context = super().get_context_data(object_list=object_list, **kwargs)
        return context

class ScanArchiveView(LoginRequiredMixin, DetailView):
    """ this view request a scan of the Archive provided by its pk """
    model = Archive
    slug_url_kwarg = 'id'

    def get(self, request, *args, **kwargs):
        """ override get() to call list_files() """
        self.user = request.user
        self.object = self.get_object()

        deep_scan = self.object.repository.deep_scan

        async_task('tbw_core.tasks.scan_archive_by_id', self.object.id, request.user.settings.id, deep_scan)

        messages.add_message(request, messages.INFO, f"Starting a scan of the archive {self.object.path}")

        return HttpResponseRedirect(reverse('repository', args=(self.object.repository.id,)))

    def get_queryset(self):
        if self.user.is_superuser:
            return Archive.objects.all()
        return Archive.objects.filter(repository__user=self.user)

class RestoreFileView(LoginRequiredMixin, View):
    """ this view request the restoration of the file archive_id / file_id """

    def get(self, request, *args, **kwargs):
        """ override get() to restore the requested file """
        self.user = request.user
        archive_id = self.kwargs['archive_id']
        file_id = self.kwargs['file_id']
        archive = get_object_or_404(Archive, pk=archive_id)
        try:
            file = archive.file_set.get(id=file_id)
            if file.local_file is None:
                file.local_file = Borg(request.user.settings).restore_file(archive, file.path)

            _dirname, filename = os.path.split(file.local_file)
            if os.path.exists(file.local_file):
                with open(file.local_file, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type='application/force-download')
                    response['Content-Disposition'] = 'inline; filename=' + filename
                    return response
            raise Http404
        except File.DoesNotExist:
            messages.add_message(request, messages.ERROR, 'Internal Server Error: unable to create temporary file !', 'danger')

        except BorgError as error:
            messages.add_message(request, messages.ERROR, 'Borg Error: ' + str(error), 'danger')

        return HttpResponseRedirect(reverse('archive', args=(archive_id,)))

    def get_queryset(self):
        if self.user.is_superuser:
            return Archive.objects.all()
        return Archive.objects.filter(repository__user=self.user)

class DeleteArchiveView(LoginRequiredMixin, DeleteView):
    """ this view requests the deletion of an archive from a repository """
    model = Archive
    template_name = 'tbw_core/delete_archive.html'

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super().get(request, args, kwargs)

    def get_queryset(self):
        if self.user.is_superuser:
            return Archive.objects.all()
        return Archive.objects.filter(repository__user=self.user)

    def get_success_url(self):
        """ override get_success_url() to return to the all repositories page """
        return reverse_lazy('repository', args=(self.object.repository.pk,))

    def post(self, request, *args, **kwargs):
        """ override post() to handle the cancel button """
        self.user = request.user

        if "cancel" in request.POST:
            url = self.get_success_url()
            return HttpResponseRedirect(url)

        return super().post(request, *args, **kwargs)

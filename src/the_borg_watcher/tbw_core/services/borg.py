"""
this module contains borg related commands
"""

from datetime import datetime
import json
import logging
import os
import shutil
import tempfile
from itertools import islice
from pathlib import Path

from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils import timezone

from tbw_core.models import Repository, Archive, File, UserSettings

from .common import get_process_output

class BorgError(Exception):
    """ exception used to raise borg issues """

class Borg():
    """ borg related services """

    def __init__(self, settings:UserSettings=UserSettings()):
        self.settings = settings

    @property
    def borg_cmd(self) -> str:
        """ cmd to call bog """
        return self.settings.borg_cmd

    def get_output(self, repo:Repository, args:[str], cwd:str=None) -> (int, str, str):
        """ prepare environment related to borg call """

        ssh_options = ""
        temp_dir = None
        if len(repo.ssh_config) > 0 or len(repo.ssh_id) > 0:
            temp_dir = tempfile.mkdtemp()
            if len(repo.ssh_config) > 0:
                ssh_config = tempfile.mktemp(prefix=temp_dir+'/')
                with open(ssh_config, mode='w'  ) as fd:
                    fd.write(repo.ssh_config.replace('\r\n', '\n'))
                ssh_options += " -F " + ssh_config
                os.chmod(ssh_config, 0o600)
            if len(repo.ssh_id) > 0:
                ssh_id = tempfile.mktemp(prefix=temp_dir+'/')
                with open(ssh_id, mode='w') as fd:
                    fd.write(repo.ssh_id.replace('\r\n', '\n'))
                    fd.write('\n')
                ssh_options += " -i " + ssh_id
                os.chmod(ssh_id, 0o600)

        env = {
            "BORG_PASSPHRASE": repo.encryption_key,
            "BORG_RSH": f"ssh {ssh_options}",
            "BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK": "yes",
            "BORG_RELOCATED_REPO_ACCESS_IS_OK": "yes",
            "BORG_DELETE_I_KNOW_WHAT_I_AM_DOING": "YES",
        }

        ret = get_process_output(args, env, cwd)
        if temp_dir is not None:
            shutil.rmtree(temp_dir)
        return ret

    def get_repository_info(self, repo:Repository) -> str:
        """ return a json string with borg list output """
        (ret, output, error) = self.get_output(repo, [self.borg_cmd, "info", "--json", repo.location])

        if ret == 0:
            repository_json = json.loads(output)
            return repository_json

        # use borg output as the error message
        raise BorgError(error.decode())

    def scan_repository(self, repo:Repository) -> [Archive]:
        """
        scan the provided repository
        return [new archives]
        """
        all_archives_json = self.get_repository_archives(repo)
        all_archives_ids = []
        new_archives = []
        for archive_json in all_archives_json:
            # check it dos not already exist
            try:
                archive = repo.archive_set.get(archive_id=archive_json['id'])
            except Archive.DoesNotExist:
                archive = Archive(archive_id=archive_json['id'], name=archive_json['name'], repository=repo)
                archive.save()
                new_archives += [archive]
            all_archives_ids += [archive_json['id']]
        # check for removed archives
        for archive in repo.archive_set.all():
            if not archive.archive_id in all_archives_ids:
                archive.delete()

        # update repository stats
        info_json = self.get_repository_info(repo)
        update_repository_info(repo, info_json)
        repo.save()

        return new_archives

    def get_repository_archives(self, repo:Repository) -> dict:
        """ return a dict with borg list output """
        cmd = [self.borg_cmd, "list", "--json", repo.location]
        if len(repo.pattern):
            cmd += ["--prefix", repo.pattern]
        (ret, output, error) = self.get_output(repo, cmd)
        if ret == 0:
            repository_json = json.loads(output)
            return repository_json['archives']

        # use borg output as the error message
        raise BorgError(error.decode())

    def import_repository(self, user:User, repo_info:dict) -> Repository:
        """ return a Repository from existing Borg repository
        Keyword arguments:
            user -- the owner of this repository
            repo_info -- all necessary information to import the repository
                more details in models.Repository
        Raises:
            BorgError
        """

        repo = Repository(
            repo_id=0,
            user=user,
            **repo_info,
        )

        # get repository info sync to check it exists
        info_json = self.get_repository_info(repo)
        repo.repo_id = info_json['repository']['id']
        repo.encryption_mode = Repository.Encryption.value_from_label(info_json['encryption']['mode'])
        update_repository_info(repo, info_json)

        return repo

    def new_repository(self, user:User, repo_info:dict) -> Repository:
        """ return a Repository from the creation a new Borg repository
        Keyword arguments:
            user -- the owner of this repository
            repo_info -- all necessary information to import the repository
                more details in models.Repository
        Raises:
            BorgError
        """

        repo = Repository(
            repo_id=0,
            user=user,
            **repo_info,
        )

        (ret, _output, error) = self.get_output(repo,
            [self.borg_cmd, 'init', '--encryption', str(Repository.Encryption(repo_info['encryption_mode']).label), repo_info['location']])

        if ret != 0:
            raise BorgError(error.decode())

        # get repository info to update repo_id
        info_json = self.get_repository_info(repo)
        repo.repo_id = info_json['repository']['id']

        return repo

    def delete_repository(self, repo:Repository) -> None:
        """
        delete the provided repository
        """
        (ret, _output, error) = self.get_output(repo, [self.borg_cmd, 'delete', repo.location])

        if ret != 0:
            raise BorgError(error.decode())

    def scan_archive(self, archive:Archive) -> None:
        """ scan an archive to get the contained files and update stats """
        paths = list(map(json.loads, self.list_files(archive)))

        # bulk insert
        objs = (File(path=path['path'], archive=archive, type=path['type']) for path in paths)
        batch_size = 100
        while True:
            batch = list(islice(objs, batch_size))
            if not batch:
                break
            try:
                File.objects.bulk_create(batch)
            except IntegrityError:
                # if some file of the batch is already existing, try to add them one by one

                for file in batch:
                    try:
                        file.save()
                    except IntegrityError:
                        # ignore duplicated file
                        pass

        # update archive stats
        info_json = self.get_archive_info(archive)
        update_archive_info(archive, info_json)
        archive.save()

    def deep_scan_archive(self, archive:Archive) -> None:
        """ task to construct files tree """
        modified_files = []
        tn_fields = ['tn_parent']

        logger = logging.getLogger('deep_scan')


        # keep in cache all directories
        directories = archive.file_set.filter(type='d')

        nb_done = 0
        nb_todo = len(archive.file_set.all())
        logger.debug('starting scan of %s, %i to scan', archive.name, nb_todo)
        for file in archive.file_set.all():
            pathlike = Path(file.path)
            try:
                parent = directories.get(path=pathlike.parent)
                # logger.debug("--%s found as parent of %s", parent.path, file.path)
                file.tn_parent = parent
                modified_files += [file]

                if len(modified_files) == 100:
                    File.objects.bulk_update(modified_files, tn_fields)
                    modified_files.clear()

            except File.DoesNotExist:
                logger.debug("--%s has no parent in db !", file.path)

            nb_done += 1
            if nb_done % 100 == 0:
                logger.debug("--scanned %i on %i", nb_done, nb_todo)

        # handle last modified files
        File.objects.bulk_update(modified_files, tn_fields)
        modified_files.clear()

        # needed after the bulk update
        logger.debug("starting update_tree")
        File.update_tree()
        logger.debug("ending scan of %s", archive.name)

        logger.debug("found roots=%s", str(archive.get_roots()))

    def delete_archive(self, archive:Archive) -> None:
        """ delete the requested archive from a repository """
        repo = archive.repository
        (ret, _output, error) = self.get_output(repo, [self.borg_cmd, "delete", archive.path])

        if ret != 0:
            raise BorgError(error.decode())

    def list_files(self, archive:Archive) -> list:
        """ return the list of files of provided archive as json """
        repo = archive.repository
        (ret, output, error) = self.get_output(repo, [self.borg_cmd, "list", archive.path, '--json-lines'])
        if ret == 0:
            paths = output.split(b'\n')
            paths = list(filter(None, paths)) # remove empty strings
            return paths

        raise BorgError(error.decode())

    def restore_file(self, archive:Archive, path:str) -> str:
        """
        restore the requested path from an archive
        if the path is a simple file, it is returned as is
        if the path is a directory, its content is zipped and returned
        """
        repo = archive.repository
        local_file = None
        tempdir = tempfile.mkdtemp()
        (ret, _output, error) = self.get_output(repo, [self.borg_cmd, 'extract', archive.path, path], cwd=tempdir)
        if ret == 0:
            # TODO handle errors
            complete_path = tempdir + '/' + path
            if os.path.isfile(complete_path):
                local_file = complete_path
            else:
                output_filename = tempdir + '/' + os.path.basename(path)
                local_file = shutil.make_archive(output_filename, 'zip', complete_path)
                shutil.rmtree(complete_path)
            return local_file

        raise BorgError(error.decode())

    def get_archive_info(self, archive:Archive) -> str:
        """ return a json string with borg list output """
        (ret, output, error) = self.get_output(archive.repository, [self.borg_cmd, "info", archive.path, "--json"])

        if ret == 0:
            repository_json = json.loads(output)
            return repository_json

        raise BorgError(error.decode())

def update_repository_info(repo:Repository, info:dict) -> None:
    """ update provided Repository with info as json """
    total_size = info['cache']['stats']['total_size']
    unique_size = info['cache']['stats']['unique_size']
    compressed_size = info['cache']['stats']['unique_csize']
    last_update = timezone.make_aware(datetime.fromisoformat(info['repository']['last_modified']))
    repo.total_size = total_size
    repo.unique_size = unique_size
    repo.compressed_size = compressed_size
    repo.last_update = last_update

def update_archive_info(archive:Archive, info:dict) -> None:
    """ update provided Archive with info as json """
    original_size = info['archives'][0]['stats']['original_size']
    compressed_size = info['archives'][0]['stats']['compressed_size']
    deduplicated_size = info['archives'][0]['stats']['deduplicated_size']
    time = timezone.make_aware(datetime.fromisoformat(info['archives'][0]['end']))
    archive.original_size = original_size
    archive.compressed_size = compressed_size
    archive.deduplicated_size = deduplicated_size
    archive.time = time

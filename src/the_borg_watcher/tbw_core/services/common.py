""" this module contains common services """

import subprocess
import tempfile

def get_process_output(args:[str], env:dict={}, cwd:str=None) -> (int, str, str):
    """ helper to get output from subprocess call """

    with tempfile.TemporaryFile() as stdout:
        with tempfile.TemporaryFile() as stderr:
            ret = subprocess.call(args, stdout=stdout, stderr=stderr, env=env, cwd=cwd)
            stdout.seek(0)
            stderr.seek(0)
            return (ret, stdout.read(), stderr.read())
    return (1, '', 'Unable to open temporary files for stdout/stderr')

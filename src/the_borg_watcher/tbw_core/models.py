"""
this module contains all Models
"""

import uuid

from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _
from django_q.models import Schedule
from treenode.models import TreeNodeModel

from . import settings as app_settings


class Repository(models.Model):
    """ a borg repository """

    class Encryption(models.TextChoices):
        NONE = 'N', _('none')
        AUTHENTICATED = 'A', _('authenticated')
        AUTHENTICATED_BLAKE2 = 'AB', _('authenticated-blake2')
        REPOKEY = 'R', _('repokey')
        KEYFILE = 'K', _('keyfile')
        REPOKEY_BLAKE2 = 'RB', _('repokey-blake2')
        KEYFILE_BLAKE2 = 'KB', _('keyfile-blake2')

        @classmethod
        def value_from_label(cls, label:str):
            index = 0
            for l in cls.labels:
                if label == l:
                    return cls.values[index]
                index += 1
            raise ValueError()

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    repo_id = models.CharField(max_length=64)
    location = models.CharField(max_length=512)
    encryption_mode = models.CharField(
        max_length=2, choices=Encryption.choices, default=Encryption.NONE)
    encryption_key = models.CharField(max_length=512, default="", blank=True)
    total_size = models.FloatField(default=0)
    unique_size = models.FloatField(default=0)
    compressed_size = models.FloatField(default=0)
    ssh_config = models.TextField(max_length=1024, default="", blank=True)
    ssh_id = models.TextField(max_length=4096, default="", blank=True)
    last_update = models.DateTimeField(blank=True, null=True)
    # pattern : used to filter archives in scan (initial and periodic)
    pattern = models.CharField(max_length=1024, default="", blank=True)
    deep_scan = models.BooleanField(default=False)

    class Meta:
        """ override default names """
        verbose_name = 'Repository'
        verbose_name_plural = 'Repositories'

    def __str__(self):
        return str(self.location)


class Archive(models.Model):
    """ a borg archive, part of a repository """
    id = models.AutoField(primary_key=True)
    archive_id = models.CharField(max_length=64)
    name = models.CharField(max_length=256)
    repository = models.ForeignKey(Repository, on_delete=models.CASCADE)
    original_size = models.FloatField(default=0)
    compressed_size = models.FloatField(default=0)
    deduplicated_size = models.FloatField(default=0)
    time = models.DateTimeField(blank=True, null=True)

    @property
    def path(self) -> str:
        """ return the complete location::archive path """
        return self.repository.location + "::" + self.name

    def get_roots_queryset(self):
        return self.file_set.filter(tn_ancestors_count=0).order_by('id')

    def get_roots(self) -> list:
        """ return the list of root files """
        return list(self.get_roots_queryset())

class File(TreeNodeModel):
    """ a file, part of an Archive """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    path = models.CharField(max_length=2048)
    archive = models.ForeignKey(Archive, on_delete=models.CASCADE)
    type = models.CharField(max_length=1) # TODO replace by an enum
    local_file = None
    treenode_display_field = 'path'

    class Meta:
        unique_together = ('path', 'archive')
        indexes = [
            models.Index(fields=['path']),
        ]

class RepositorySchedule(Schedule):
    """ a schedule object associated to a repository """
    repository = models.ForeignKey(Repository, on_delete=models.CASCADE)


class UserSettings(models.Model):
    """ app settings per user """
    id = models.AutoField(primary_key=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='settings', null=True)
    archives_per_page = models.IntegerField(default=10)
    files_per_page = models.IntegerField(default=10)
    borg_cmd = models.CharField(max_length=256, default='borg')


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_settings(sender, instance, created, **kwargs):
    """" signal receiver to create a UserSettings object upon User creation """
    if created:
        UserSettings.objects.create(
            user=instance, borg_cmd=app_settings.BORG_PATH)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def save_user_settings(sender, instance, **kwargs):
    """ signal to save the UserSettings object upon User save """
    instance.settings.save()

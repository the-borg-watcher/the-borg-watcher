"""
this module contains tasks that can be scheduled
"""

from django_q.tasks import async_task

from tbw_core.models import Repository, Archive, UserSettings
from tbw_core.services import Borg

def scan_repository_by_id(repo_id:int, settings_id:int) -> None:
    """ task that call scan_repository and then plan a scan of new archives """

    repo = Repository.objects.get(pk=repo_id)
    settings = UserSettings.objects.get(pk=settings_id)
    new_archives = Borg(settings).scan_repository(repo)

    for archive in new_archives:
        # schedule a one shot scan of new archives
        async_task('tbw_core.tasks.scan_archive_by_id',
            archive.id,
            settings_id,
            repo.deep_scan,
        )

def delete_repository_by_id(repo_id:int, settings_id:int, delete_on_disk:bool) -> None:
    """ task that delete a repository by its id """

    repo = Repository.objects.get(pk=repo_id)
    settings = UserSettings.objects.get(pk=settings_id)

    if delete_on_disk:
        Borg(settings).delete_repository(repo)

    repo.delete()

def scan_archive_by_id(archive_id:int, settings_id:int, deep_scan:bool) -> None:
    """ task to call scan_archive """
    archive = Archive.objects.get(pk=archive_id)
    settings = UserSettings.objects.get(pk=settings_id)
    Borg(settings).scan_archive(archive)
    if deep_scan:
        Borg(settings).deep_scan_archive(archive)

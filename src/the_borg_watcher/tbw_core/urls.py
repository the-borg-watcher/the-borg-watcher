from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('repositories/', views.AllRepositoriesView.as_view(), name='all_repositories'),
    path('repository/import/', views.ImportRepositoryView.as_view(), name='import_repository'),
    path('repository/new/', views.NewRepositoryView.as_view(), name='new_repository'),
    path('repository/<str:pk>/', views.RepositoryView.as_view(), name='repository'),
    path('repository/<str:pk>/edit/', views.EditRepositoryView.as_view(), name='edit_repository'),
    path('repository/<str:pk>/delete/', views.DeleteRepositoryView.as_view(), name='delete_repository'),
    path('repository/<str:pk>/scan/', views.ScanRepositoryView.as_view(), name='scan_repository'),
    path('archive/<str:pk>/', views.ArchiveView.as_view(), name='archive'),
    path('archive/<str:pk>/scan/', views.ScanArchiveView.as_view(), name='scan_archive'),
    path('archive/<str:pk>/delete/', views.DeleteArchiveView.as_view(), name='delete_archive'),
    path('archive/<str:archive_id>/file/<str:file_id>/', views.RestoreFileView.as_view(), name='restore_file'),
    path('settings/edit', views.EditSettingsView.as_view(), name='edit_settings'),
]

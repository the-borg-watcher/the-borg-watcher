from django.contrib import admin

# Register your models here.

from .models import Repository, Archive, File, UserSettings

admin.site.register(Archive)
admin.site.register(File)
admin.site.register(Repository)
admin.site.register(UserSettings)

"""
this module contains customized forms
"""

from django import forms

from .models import Repository

class ImportRepositoryForm(forms.ModelForm):

    class Meta:
        model = Repository
        fields = ['location', 'encryption_key', 'pattern', 'deep_scan', 'ssh_config', 'ssh_id']
        widgets = {
            'encryption_key': forms.PasswordInput(),
        }

class RepositoryForm(forms.ModelForm):

    class Meta:
        model = Repository
        fields = ['location', 'encryption_mode', 'encryption_key', 'pattern', 'deep_scan', 'ssh_config', 'ssh_id']
        widgets = {
            'encryption_key': forms.PasswordInput(),
        }

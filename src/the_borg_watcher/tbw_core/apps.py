from django.apps import AppConfig


class TBWConfigConfig(AppConfig):
    name = 'tbw_core'

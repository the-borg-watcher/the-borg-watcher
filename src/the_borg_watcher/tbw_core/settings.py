from django.conf import settings

if settings.DEBUG:
    BORG_PATH='borg'
else:
    BORG_PATH='/env/bin/borg'

"""
this module contains the tests for views
"""

import os
import shutil
import tempfile

from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from tbw_core.models import Repository, Archive
from tbw_core.services import Borg

from . import helpers

class TestAllRepositoriesView(TestCase):
    """ testcase for AllRepositoriesView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.admin = User.objects.create_superuser('admin', 'admin@test.com', 'admin')

    def test_not_logged_in(self):
        """ test redirect when not logged in """
        response = self.client.get('/')

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, "/accounts/login/?next=/")

    def test_no_repo(self):
        """ test content when no repo is available """

        self.client.login(username='john', password='john')
        response = self.client.get('/')

        self.assertEqual(len(response.context['repository_list']), 0)

    def test_one_repo(self):
        """ test content when one repo is in the DB """
        Repository.objects.create(
            user=self.john,
            repo_id=0,
            location='some_location',
            encryption_key='',
            ssh_config='',
        )

        self.client.login(username='john', password='john')
        response = self.client.get('/')

        self.assertEqual(len(response.context['repository_list']), 1)

    def test_user_check(self):
        """ test john can't see admin repos """
        Repository.objects.create(
            user=self.admin,
            repo_id=0,
            location='some_location',
            encryption_key='',
            ssh_config='',
        )

        self.client.login(username='john', password='john')
        response = self.client.get('/')

        self.assertEqual(len(response.context['repository_list']), 0)

class TestRepositoryView(TestCase):
    """ testcase for RepositoryView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.admin = User.objects.create_superuser('admin', 'admin@test.com', 'admin')

    def test_valid_id(self):
        """ test rendering of a valid repo """

        repo = Repository.objects.create(
            user=self.john,
            repo_id=0,
            location='some_location',
            encryption_key='',
            ssh_config='',
            ssh_id='',
        )
        archives = []
        for i in range(0, 20):
            archives += [Archive.objects.create(
                archive_id=i,
                name='my_archive_'+str(i),
                repository=repo
            )]

        self.client.login(username='john', password='john')
        response = self.client.get(reverse('repository', kwargs={'pk': repo.id}))

        self.assertEqual(response.context['repository'].location, 'some_location')

        # check for pagination
        self.assertEqual(len(response.context['object_list']), self.john.settings.archives_per_page)

    def test_user_check(self):
        """ test john can't see admin repository """

        repo = Repository.objects.create(
            user=self.admin,
            repo_id=0,
            location='some_location',
            encryption_key='',
            ssh_config='',
            ssh_id='',
        )
        archives = []
        for i in range(0, 20):
            archives += [Archive.objects.create(
                archive_id=i,
                name='my_archive_'+str(i),
                repository=repo
            )]

        self.client.login(username='john', password='john')
        response = self.client.get(reverse('repository', kwargs={'pk': repo.id}))

        self.assertEqual(response.status_code, 404)

class TestImportRepositoryView(TestCase):
    """ testcase for ImportRepositoryView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')

    def test_import_valid(self):
        """ test import ok """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)

        self.client.login(username='john', password='john')
        response = self.client.post(reverse('import_repository'),
            data={'location': repo_dir, 'encryption_mode': 'N', 'encryption_key': '', 'ssh_config': '', 'ssh_id': ''}
            )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('repository', kwargs={'pk': 1}))

        shutil.rmtree(repo_dir)
        os.unlink(file)

class TestNewRepositoryView(TestCase):
    """ testcase for NewRepositoryView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')

    def test_new_valid(self):
        """ test creation ok """

        tempdir = tempfile.mkdtemp()

        self.client.login(username='john', password='john')
        response = self.client.post(reverse('new_repository'),
            data={'location': tempdir, 'encryption_mode': 'N', 'encryption_key': '', 'ssh_config': '', 'ssh_id': ''}
            )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, reverse('repository', kwargs={'pk': 1}))

        self.assertTrue(os.path.isfile(tempdir + '/README'))

        shutil.rmtree(tempdir)

class TestDeleteRepositoryView(TestCase):
    """ testcase for DeleteRepositoryView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.admin = User.objects.create_superuser('admin', 'admin@test.com', 'admin')

    def test_delete_valid(self):
        """ test delete valid """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)

        self.client.login(username='john', password='john')

        # with no delete
        repo = Borg().import_repository(self.john, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        repo_id = repo.id

        response = self.client.post(reverse('delete_repository', kwargs={'pk': repo_id}),
            data={'delete_on_disk': 'off'}
            )
        self.assertEqual(response.status_code, 302)

        self.assertTrue(os.path.isfile(repo_dir + '/README'))
        self.assertRaises(Repository.DoesNotExist, Repository.objects.get, pk=repo_id)

        # with deletion
        repo = Borg().import_repository(self.john, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        repo_id = repo.id

        response = self.client.post(reverse('delete_repository', kwargs={'pk': repo_id}),
            data={'delete_on_disk': 'on'}
            )
        self.assertEqual(response.status_code, 302)

        self.assertFalse(os.path.isfile(repo_dir + '/README'))
        self.assertRaises(Repository.DoesNotExist, Repository.objects.get, pk=repo_id)

        if os.path.isdir(repo_dir):
            shutil.rmtree(repo_dir)
        if os.path.isdir(file):
            os.unlink(file)

    def test_delete_check_user(self):
        """ test delete valid """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)


        repo = Borg().import_repository(self.admin, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        repo_id = repo.id

        self.client.login(username='john', password='john')

        response = self.client.post(reverse('delete_repository', kwargs={'pk': repo_id}),
            data={'delete_on_disk': 'off'}
            )
        self.assertEqual(response.status_code, 404)

class TestScanRepositoryView(TestCase):
    """ testcase for ScanRepositoryView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')

    def test_scan_valid(self):
        """ test delete valid """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)

        self.client.login(username='john', password='john')

        # with no delete
        repo = Borg().import_repository(self.john, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        response = self.client.get(reverse('scan_repository', kwargs={'pk': repo.id}))
        self.assertEqual(response.status_code, 302)


        if os.path.isdir(repo_dir):
            shutil.rmtree(repo_dir)
        if os.path.isdir(file):
            os.unlink(file)

class TestArchiveView(TestCase):
    """ testcase for ArchiveView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.admin = User.objects.create_superuser('admin', 'admin@test.com', 'admin')

    def test_check_user(self):

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)


        repo = Borg().import_repository(self.admin, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)

        self.client.login(username='john', password='john')

        response = self.client.get(reverse('archive', kwargs={'pk': archives[0].id}))
        self.assertEqual(response.status_code, 404)


        if os.path.isdir(repo_dir):
            shutil.rmtree(repo_dir)
        if os.path.isdir(file):
            os.unlink(file)

class TestDeleteArchiveView(TestCase):
    """ testcase for DeleteArchiveView """

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.admin = User.objects.create_superuser('admin', 'admin@test.com', 'admin')

    def test_check_user(self):

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)


        repo = Borg().import_repository(self.admin, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)

        self.client.login(username='john', password='john')

        response = self.client.get(reverse('delete_archive', kwargs={'pk': archives[0].id}))
        self.assertEqual(response.status_code, 404)


        if os.path.isdir(repo_dir):
            shutil.rmtree(repo_dir)
        if os.path.isdir(file):
            os.unlink(file)


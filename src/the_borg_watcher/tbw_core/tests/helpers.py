"""
this module contains the helpers for tests
"""

import docker
import os
import random
import shutil
import string
import tempfile
import time

from tbw_core.services import borg


def unique_strings(length:int) -> str:
    """ generate a random ascii string """

    return str(random.choices(string.ascii_letters, k=length))

def create_local_repo(encryption:str='none', encryption_key:str='') -> str:
    """ helper that create a local borg repo """

    env = {"BORG_PASSPHRASE": encryption_key}

    repo_dir = tempfile.mkdtemp()

    (ret, _output, _error) = borg.get_process_output(
        ['borg', 'init', '--encryption', encryption, repo_dir],
        env,
    )

    if ret != 0:
        shutil.rmtree(repo_dir)
        return None
    return repo_dir

def create_some_file(length:int) -> (str, str):
    """ helper that create some dummy file """

    some_file = tempfile.mktemp()
    content = unique_strings(length)
    with open(some_file, 'w') as writer:
        writer.write(content)

    return (some_file, content)

def create_archive(repo_dir:str, archive:str, encryption_key:str, *args, **kwargs) -> None:
    """ helper that create an archive """

    env = {"BORG_PASSPHRASE": encryption_key}

    (ret, _output, error) = borg.get_process_output(
        ['borg', 'create', repo_dir + '::' + archive, *args],
        env,
    )

    if ret != 0:
        raise borg.BorgError(error.decode())

class BorgSshServer():
    server_image = os.getenv('DOCKER_IMAGE_FULL_PATH', 'registry.gitlab.com/the-borg-watcher/borg-server/borg-server:latest')

    def __init__(self, name:str):
        self.name = name
        self.docker_client = docker.from_env()
        self.temp_dir = tempfile.mkdtemp()
        self.container = self.docker_client.containers.run(self.server_image, detach=True,
         volumes={self.temp_dir: {'bind': '/mnt/repo', 'mode': 'rw'}},
         environment={'SSHD_OPTIONS': '-o LogLevel=DEBUG2'})
        time.sleep(2)
        self.ssh_private = self.container.exec_run('cat /root/.ssh/id_rsa').output.decode()
        self.ssh_public = self.container.exec_run('cat /root/.ssh/id_rsa.pub').output.decode()
        self.container.reload()
        self.ip_address = self.container.attrs['NetworkSettings']['IPAddress']

    def __del__(self):
        # do delete in container as root first
        self.container.exec_run('rm -fr /mnt/repo/')
        self.container.stop()
        self.container.remove()
        shutil.rmtree(self.temp_dir)

    @property
    def location(self) -> str:
        return f"root@{self.name}:/mnt/repo"

    def authorize_key(self, key:str) -> None:
        print("keyrs="+str(self.container.exec_run("cat /root/.ssh/authorized_keys").output))
        self.container.exec_run(f"echo {key} >> /root/.ssh/authorized_keys")
        print("keyrs="+str(self.container.exec_run("cat /root/.ssh/authorized_keys").output))

"""
this module contains the tests of services
"""

import os
import os.path
import shutil
import tempfile

from django.contrib.auth.models import User
from django.test import TransactionTestCase

from tbw_core.services import Borg
from tbw_core.models import Repository, UserSettings

from tbw_core.tasks import *

from . import helpers

class TasksTestCase(TransactionTestCase):

    def setUp(self):
        self.john = User.objects.create_user('john', 'john@test.com', 'john')
        self.client.login(username='john', password='john')
        self.settings = UserSettings.objects.get(user=self.john)

    def test_scan_repository_by_id(self):

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)
        helpers.create_archive(repo_dir, 'foo2', '', temp1)

        repo = Borg().import_repository(self.john, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = repo.archive_set.all()

        scan_repository_by_id(repo.id, self.settings.id)

        archives = repo.archive_set.all()

        self.assertEqual(len(archives), 2)
        self.assertEqual(archives[0].name, 'foo')
        self.assertEqual(archives[1].name, 'foo2')

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_delete_repository_by_id(self):
        temp1 = tempfile.mkdtemp()

        # with delete
        repo = Borg().new_repository(
            self.john,
            {'location':temp1, 'encryption_mode':'N', 'encryption_key': '', 'ssh_config': '', 'ssh_id': '', 'pattern': ''}
        )
        repo.save()
        repo_id = repo.id

        delete_repository_by_id(repo_id, self.settings.id, delete_on_disk=True)

        self.assertFalse(os.path.isfile(temp1 + '/README'))

        self.assertRaises(Repository.DoesNotExist, Repository.objects.get, pk=repo_id)

        # with NO delete
        repo = Borg().new_repository(
            self.john,
            {'location': temp1, 'encryption_mode': 'N', 'encryption_key': '', 'ssh_config': '', 'ssh_id': '', 'pattern': ''}
        )
        repo.save()
        repo_id = repo.id

        delete_repository_by_id(repo_id, self.settings.id, delete_on_disk=False)

        self.assertTrue(os.path.isfile(temp1 + '/README'))

        self.assertRaises(Repository.DoesNotExist, Repository.objects.get, pk=repo_id)

        if os.path.isdir(temp1):
            shutil.rmtree(temp1)

    def test_def_scan_archive_by_id(self):

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        temp1_file1 = temp1 + '/' + os.path.basename(file1)
        temp1_file2 = temp1 + '/' + os.path.basename(file2)
        shutil.move(file1, temp1_file1)
        shutil.move(file2, temp1_file2)

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.john, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)
        archive = archives[0]


        scan_archive_by_id(archive.id, self.settings.id, True)


        files = archive.file_set.all()

        self.assertEqual(len(files), 3)

        # remove heading /
        expected_files = [temp1[1:], temp1_file1[1:], temp1_file2[1:]]
        found_files = 0

        for file in files:
            if file.path in expected_files:
                found_files += 1
            else:
                print('file='+file.path)

        self.assertEqual(found_files, 3)

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)


"""
this module contains all tests
"""

from . import helpers
from .test_services import *
from .test_tasks import *
from .test_views import *

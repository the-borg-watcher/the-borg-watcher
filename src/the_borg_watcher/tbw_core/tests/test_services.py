"""
this module contains the tests of services
"""

import filecmp
import json
import os
import os.path
import shutil
import tempfile

from django.contrib.auth.models import User
from django.test import TransactionTestCase
from django.test import tag

from tbw_core.services import Borg, BorgError, borg
from tbw_core.models import Repository, Archive

from . import helpers

class ServiceBorgTestCase(TransactionTestCase):
    """ testcase for all borg service related """

    def setUp(self):
        self.user = User(username='admin', password='admin')
        self.user.save()

    def test_get_repository_info_local_no_encrypt(self):
        """ test local with no encryption """

        repo_dir = helpers.create_local_repo()

        repo = Borg().import_repository(self.user, {
            'location': repo_dir,
            'encryption_key': '',
            'ssh_config': '',
            'ssh_id': '',
            'pattern': '',
            }
        )
        self.assertIsInstance(repo, Repository)

        shutil.rmtree(repo_dir)

    def test_get_repository_info_local_not_exist(self):
        """ test not existing locations """

        self.assertRaises(
            BorgError,
            Borg().import_repository,
            self.user, {'location':'/tmp/foo/bar', 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''}
        )

    def test_get_repository_info_local_repokey(self):
        """ test local with repokey encryption """

        repo_dir = helpers.create_local_repo(encryption='repokey', encryption_key='zoo')

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'zoo', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        self.assertIsInstance(repo, Repository)

        shutil.rmtree(repo_dir)

    def test_repository_archives_empty(self):
        """ test get archives from an empty repository """

        repo_dir = helpers.create_local_repo(encryption='none')

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        archives = Borg().get_repository_archives(repo)

        self.assertEqual(len(archives), 0)

        shutil.rmtree(repo_dir)

    def test_repository_archives(self):
        """ test get archives from repository """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        archives = Borg().get_repository_archives(repo)

        self.assertEqual(archives[0]['archive'], 'foo')

        shutil.rmtree(repo_dir)
        os.unlink(file)

    def test_repository_archives_removed(self):
        """ test get archives from an obsolete repository """

        (file, _content) = helpers.create_some_file(42)
        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        shutil.rmtree(repo_dir)

        self.assertRaises(
            BorgError,
            Borg().get_repository_archives, repo
        )

        os.unlink(file)

    def test_list_files(self):
        """ test list_files """

        all_files = []
        (file1, _content) = helpers.create_some_file(42)
        all_files += [file1[1:]] # remove leading /
        (file2, _content) = helpers.create_some_file(42)
        all_files += [file2[1:]] # remove leading /

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file1, file2)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        files = list(map(json.loads, Borg().list_files(archive)))

        for file in files:
            self.assertIn(file['path'], all_files)

        os.unlink(file1)
        os.unlink(file2)
        shutil.rmtree(repo_dir)

    def test_list_files_obsolete(self):
        """ test list_files for a remove repository """

        (file1, _content) = helpers.create_some_file(42)
        (file2, _content) = helpers.create_some_file(42)

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file1, file2)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        os.unlink(file1)
        os.unlink(file2)
        shutil.rmtree(repo_dir)

        self.assertRaises(
            BorgError,
            Borg().list_files, archive
        )

    def test_restore_file(self):
        """ test restore_file for a simple file """

        (file1, content) = helpers.create_some_file(42)
        (file2, _content) = helpers.create_some_file(42)

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file1, file2)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        # root / is removed from archived files, so removed it here too
        restored_file = Borg().restore_file(archive, file1[1:])

        with open(restored_file, 'r') as fd:
            restored_content = fd.read()

        self.assertEqual(content, restored_content)

        os.unlink(file1)
        os.unlink(file2)
        shutil.rmtree(repo_dir)

    def test_restore_file_not_existing(self):
        """ test restore_file for a wrong path """

        (file1, _content) = helpers.create_some_file(42)
        (file2, _content) = helpers.create_some_file(42)

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', file1, file2)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        self.assertRaises(
            BorgError,
            Borg().restore_file, archive, 'foo'
        )

        os.unlink(file1)
        os.unlink(file2)
        shutil.rmtree(repo_dir)

    def test_restore_file_directory(self):
        """ test restore_file for a directory """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        # root / is removed from archived files, so removed it here too
        restored_zip = Borg().restore_file(archive, temp1[1:])

        temp2 = tempfile.mkdtemp()
        shutil.unpack_archive(restored_zip, temp2)

        self.assertTrue(filecmp.dircmp(temp1, temp2))

        shutil.rmtree(temp1)
        shutil.rmtree(temp2)
        shutil.rmtree(repo_dir)

    def test_get_archive_info(self):
        """ test get_archive_info """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        info_json = Borg().get_archive_info(archive)

        name = info_json['archives'][0]['name']
        self.assertEqual(name, 'foo')

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_get_archive_info_obsolete(self):
        """ test get_archive_info for a removed repository """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        shutil.rmtree(repo_dir)

        self.assertRaises(
            BorgError,
            Borg().get_archive_info, archive,
        )

        shutil.rmtree(temp1)

    def test_update_archive_info(self):
        """ test get_archive_info """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()
        archives = Borg().get_repository_archives(repo)
        archive = Archive(archive_id=archives[0]['id'], name=archives[0]['name'], repository=repo)
        archive.save()

        info_json = Borg().get_archive_info(archive)
        borg.update_archive_info(archive, info_json)
        archive.save()

        self.assertTrue(archive.original_size > 0)
        self.assertTrue(archive.compressed_size > 0)
        self.assertTrue(archive.deduplicated_size > 0)

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_scan_repository(self):
        """ test scan_repository """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)
        helpers.create_archive(repo_dir, 'foo2', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)

        self.assertEqual(len(archives), 2)
        self.assertEqual(archives[0].name, 'foo')
        self.assertEqual(archives[1].name, 'foo2')

        helpers.create_archive(repo_dir, 'foo3', '', temp1)

        archives = Borg().scan_repository(repo)

        self.assertEqual(len(archives), 1)
        self.assertEqual(archives[0].name, 'foo3')


        # test import_repository with a pattern
        helpers.create_archive(repo_dir, 'bar', '', temp1)

        repo2 = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':'foo'})
        repo2.save()

        archives = Borg().scan_repository(repo2)

        # check bar is not scanned
        self.assertEqual(len(archives), 3)
        self.assertEqual(archives[0].name, 'foo')
        self.assertEqual(archives[1].name, 'foo2')
        self.assertEqual(archives[2].name, 'foo3')


        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_delete_archive(self):
        """ test delete_archive """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)
        helpers.create_archive(repo_dir, 'foo2', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)

        self.assertEqual(len(repo.archive_set.all()), 2)

        Borg().delete_archive(archives[1])
        Borg().scan_repository(repo)

        self.assertEqual(len(repo.archive_set.all()), 1)

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_delete_archive_failure(self):
        """ test delete_archive """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        shutil.move(file1, temp1 + '/' + os.path.basename(file1))
        shutil.move(file2, temp1 + '/' + os.path.basename(file2))

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)
        helpers.create_archive(repo_dir, 'foo2', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)

        self.assertEqual(len(repo.archive_set.all()), 2)

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

        self.assertRaises(BorgError, Borg().delete_archive, archives[1])

    def test_scan_archive(self):
        """ test scan_archive """

        (file1, _content1) = helpers.create_some_file(42)
        (file2, _content2) = helpers.create_some_file(42)

        temp1 = tempfile.mkdtemp()
        temp1_file1 = temp1 + '/' + os.path.basename(file1)
        temp1_file2 = temp1 + '/' + os.path.basename(file2)
        shutil.move(file1, temp1_file1)
        shutil.move(file2, temp1_file2)

        repo_dir = helpers.create_local_repo(encryption='none')
        helpers.create_archive(repo_dir, 'foo', '', temp1)

        repo = Borg().import_repository(self.user, {'location': repo_dir, 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''})
        repo.save()

        archives = Borg().scan_repository(repo)
        archive = archives[0]

        Borg().scan_archive(archive)
        files = archive.file_set.all()

        self.assertEqual(len(files), 3)

        # remove heading /
        expected_files = [temp1[1:], temp1_file1[1:], temp1_file2[1:]]
        found_files = 0

        for file in files:
            if file.path in expected_files:
                found_files += 1
            else:
                print('file='+file.path)

        self.assertEqual(found_files, 3)

        # try a second scan to check we still have only 3 files
        Borg().scan_archive(archive)
        files = archive.file_set.all()

        self.assertEqual(len(files), 3)

        # remove heading /
        expected_files = [temp1[1:], temp1_file1[1:], temp1_file2[1:]]
        found_files = 0

        for file in files:
            if file.path in expected_files:
                found_files += 1
            else:
                print('file='+file.path)

        self.assertEqual(found_files, 3)

        shutil.rmtree(temp1)
        shutil.rmtree(repo_dir)

    def test_new_repository(self):

        temp1 = tempfile.mkdtemp()

        repo = Borg().new_repository(self.user,
            {'location': temp1, 'encryption_mode':'N', 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''},
        )
        repo.save()

        self.assertTrue(os.path.isfile(temp1 + '/README'))

        shutil.rmtree(temp1)

    def test_new_repository_failure(self):

        temp1 = tempfile.mkdtemp()

        self.assertRaises(ValueError, Borg().new_repository, self.user,
            {'location': temp1, 'encryption_mode':'foo', 'encryption_key':'', 'ssh_config':'', 'ssh_id':'', 'pattern':''},
        )

        shutil.rmtree(temp1)

    # this test is KO on CI
    @tag('no-ci')
    def test_new_remote(self):

        remote = helpers.BorgSshServer('remote_server')

        location = remote.location
        ssh_config = f"""
Host {remote.name}
Hostname {remote.ip_address}
StrictHostKeyChecking no
"""
        ssh_id = remote.ssh_private

        repo = Borg().new_repository(self.user,
            {'location': location, 'encryption_mode':'N', 'encryption_key':'', 'ssh_config':ssh_config, 'ssh_id':ssh_id, 'pattern':''},
        )
        repo.save()

        self.assertTrue(os.path.isfile(remote.temp_dir + '/README'))

#!/bin/sh

# no error
set -e

# generate host key
ssh-keygen -A

# generate root user key
ssh-keygen -t rsa -N '' -f ~/.ssh/id_rsa
cp ~/.ssh/id_rsa.pub ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

# allow root to connect
passwd -d root

# run sshd
/usr/sbin/sshd -D ${SSHD_OPTIONS} -p ${SSHD_PORT}

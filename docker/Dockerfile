FROM python:3.9-alpine

ADD src/the_borg_watcher/requirements.txt /app/requirements.txt
ADD src/the_borg_watcher/requirements_runtime.txt /app/requirements_runtime.txt

RUN set -ex \
    && apk add --no-cache --virtual .build-deps openssl-dev acl-dev linux-headers postgresql-dev build-base \
    && python -m venv /env \
    && /env/bin/pip install --upgrade pip wheel \
    && /env/bin/pip install --no-cache-dir -r /app/requirements_runtime.txt \
    && runDeps="$(scanelf --needed --nobanner --recursive /env \
        | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
        | sort -u \
        | xargs -r apk info --installed \
        | sort -u)" \
    && apk add --virtual rundeps $runDeps \
    && apk del .build-deps \
    && rm /app/requirements*.txt
RUN ln -s /env/bin/borg /usr/local/bin

RUN apk add --no-cache openssh-client

ADD src/the_borg_watcher /app
WORKDIR /app

# set environment variables
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV PORT 8000
EXPOSE ${PORT}

ENV WORKERS 4
ENV ASYNC_WORKERS 4

ENV DJANGO_SUPERUSER_USERNAME admin
ENV DJANGO_SUPERUSER_PASSWORD admin
ENV DJANGO_SUPERUSER_EMAIL admin@tbw.com

ENV DJANGO_DEBUG "False"

ADD docker/Procfile /app
ADD docker/entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD ["web", "worker"]

#!/bin/sh

# no error
set -e

cd /app

# get the static files
./manage.py collectstatic --clear --noinput

# update database
./manage.py migrate

# crate a superuser if none is available
./manage.py createsuperuser --noinput || true

# start honcho
honcho start $@
